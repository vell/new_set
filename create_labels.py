import json
from PIL import Image


f = open('dataset/train/via_region_data.json', 'r')
d = json.loads(f.read())
# with open('logs/train.txt', 'w') as f_train:
for k, v in d.items():
    # f_train.write(f"/Users/vell/projects/new_set/dataset/images/{v['filename']}\n")

    with Image.open(f'dataset/images/{v["filename"]}') as img:
        (w, h) = img.size
        kw, kh = 1/w, 1/h
    with open(f'dataset/train/{v["filename"][:-4]}.txt', 'w') as f_image:
        for value in v['regions']:
            x = min(value['shape_attributes']['all_points_x'])
            xd = max(value['shape_attributes']['all_points_x'])
            y = min(value['shape_attributes']['all_points_y'])
            yd = max(value['shape_attributes']['all_points_y'])
            f_image.write(f'0 {(x + (xd - x) / 2) * kw} {(y + (yd - y) / 2) * kh} {(xd - x) * kw} {(yd - y) * kh}\n')

    #     x = value['shape_attributes']['x']
        #     w = value['shape_attributes']['width']
        #     y = value['shape_attributes']['y']
        #     h = value['shape_attributes']['height']
        #     f_image.write(f'0 {(x + w / 2) * kw} {(y + h / 2) * kh} {w * kw} {h * kh}\n')
